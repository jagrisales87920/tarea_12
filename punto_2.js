class cuenta {
    constructor(titular, saldo) {
        this.titular = titular
        this.cantidad = saldo
    }
    mostrar() {
        console.log(`Titular: ${this.titular} Saldo: ${this.cantidad}`)
    }
    ingresar(valor) {
        if (valor > 0) {
            this.cantidad += valor; // this.cantidad = this.cantidad + valor
            console.log("Consignación exitosa")
        }
        if (valor <= 0) {
            console.log("No se pudo procesar tu solicitud.")
            console.log("El valor ingresado no es valido.")
        }
    }
    retirar(valor) {
        if (valor <= this.cantidad) {
            this.cantidad -= valor;
            console.log("Retiro exitoso")
            console.log(`Tu saldo actual es : ${this.cantidad}`)
        }
        if (valor > this.cantidad) {
            console.log("Saldo insuficiente")
            console.log(`Tu saldo es de : ${this.cantidad}`)
        }
    }

}
let cuentaDaniel = ["julian", 100000]
let cuenta1 = new cuenta(cuentaDaniel[0], cuentaDaniel[1])
cuenta1.ingresar(50000)
cuenta1.retirar(45000)
cuenta1.mostrar()